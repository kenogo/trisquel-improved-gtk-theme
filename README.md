# Trisquel improved GTK theme

This improves on Trisquel's theme by making the GTK3 window buttons consistent with those of GTK2. It also removes the round corners of GTK3 titlebars to provide even more consistency. Just plug `Trisquel-improved` into your `~/.local/share/themes` and `trisquel-improved` into `~/.local/share/icons` (create these directories if they don't yet exist). This theme should then show up as "Trisquel improved" under "Appearance" in your system control.
